# Kelas Software Engineer Wiki

_When in doubt, please RTFM_

* [Definition of Wiki](https://en.wikipedia.org/wiki/Wiki)
* [Table of Contents](SUMMARY.md)
* [Frequently Asked Questions](README.md#faq)

Repository: https://gitlab.com/kelassoftwareengineer/kelassoftwareengineer.gitlab.io

Hosted on https://kelassoftwareengineer.gitlab.io

## Preface

We always start at zero. We don't know anything when we come to this world. But humanity still exists until now because our ancestors passed down their wisdom and knowledge to the future generation. They write down what they hear, what they see, what they smell, and what they feel. Therefore we indeed still start from zero. But not the same level zero with our ancestors, we continue what they left off.

![Egyptians Hieroglyphs](image/egyptians-hieroglyphs.jpg "Dunno what this means, but it is cool anyway lol")

We believe that it also applies to Sofware Engineering. We write down everything that can be documented. It can be how-to-do-something, contract with PM, root cause of an issue, and many more.

We want to achieve fast and we want it faster. But there's a thing called ["The curse of knowledge"](https://en.wikipedia.org/wiki/Curse_of_knowledge) - We assume that everyone should have known what we know. When there's a difference in knowledge on the team, it can slow down the progress. Without a document, you need someone to transfer the knowledge when there is a new person join in. But with a good Wiki, we can cut off months of learning to weeks. you don't need to tell him an answer for many questions that have been asked before, you may not miss something because all things that should be known already written, and if anyone who finds this is still lack of info, then it will get updated. Indeed, we will still need to help the new hire, but it's only a small portion and better than nothing, right?

> The final job for a master is to find his successor

By having this Wiki, we hope that you (participants) can explore this as a practice to contribute something to the community. Giving information that can be useful for future generations.

Some interesting videos to watch:

- ["[Bahasa] Organisasi Butuh Data"](https://youtu.be/6zOsSiKQZXI) by Saiful Islam (Cloud Engineer of Bukalapak) in Himakom PSDM Friday Open Mic #3

- ["[English] The World Full of Idiots"](https://www.youtube.com/watch?v=FA0mmD0Y8cI) by Ajey Gore (CTO of Gojek) in Agile Indonesia Conference 2017

- *You can add more here* 😉

With ♥️ from Ciwaruga

🐧

## FAQ

### What is Kelas Software Engineer

**Kelas Software Engineer** is a sharing session from alumni of JTK Polban collaborating with Risetdikti Himakom.
We want to introduce what is happening in the current software industry to students by explaining the conventions and best practices.

### Who are the participants

The participants are maximum 30 students of JTK Polban. Most of them are currently in their 2nd, 3rd and 4th year in college.

### When and where Kelas Software Engineer will be held?

It’s going to be held every Saturday on 9.00-10.30 in November and December.
The venue would be in Ruang Serba Guna of Gedung Jurusan Komputer dan Teknik Informatika Politeknik Negeri Bandung

### Who are the mentors

- Ali Qornan Jaisyurrahman - Product Engineer @ Gojek

- Muhammad Imam Fauzan - DevOps Engineer @ Bukalapak

- Fajar Garnadi - Backend Engineer @ Grab

- Muhammad Saiful Islam - Cloud Engineer @ Bukalapak

- Mughie Arief Mughoni - Backend Developer @ Prudential Indonesia

- Bicky Eric Kantona - Backend Engineer @ Bukalapak

- Asep Wildan Firmansyah - Software Engineer @ Bhinneka.com

- Mohammad Hamzhya Salsatinnov Hairy - iOS Developer @ ECNET Malaysia

- Rahmat Zulfikri - Mobile Developer @ Virtual Spirit

- Muhammad Ridwan Fathin - Software Engineer @ Tokopedia

### What topics will we discuss about

| Dates                 | Topic                               | Mentor                    |
|-----------------------|-------------------------------------|---------------------------|
| Saturday, 16 November | Talk: Intro to Software Engineering | Hamzhya, Mughie, Asep     |
| Saturday, 16 November | Agile Games                         | Ali, Mughie, Asep, Rahmat |
| Saturday, 23 November | Test Driven Development             | Saiful, Mughie, Asep      |
| Saturday, 30 November | Clean Code                          | Fathin, Mughie, Asep      |
| Saturday, 7 December  | OOP Principles                      | Ali, Mughie, Fajar        |
| Saturday, 14 December | CI/CD                               | Saiful, Imam, Bicky       |
