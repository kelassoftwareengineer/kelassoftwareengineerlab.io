# Non Negotiable Etiquette

If you violate one, rm -rf! never love your code </3

---

1. Indentation and spacing between code constructs (classes/methods/tests) must be consistent.

2. Use only spaces (no tabs) for indentation.

3. Follow accepted naming conventions for your language/framework.

4. Follow accepted naming file and eirectory structure for your language/framework.

5. Use .gitignore.

6. Ensure there is a README.md that includes problem description,
dev environment setup, build instructions, and run instructions.

7. Test Driven Development (this should show in clear pattern
in the commit log - one spec, one code change per commit)

## Note

The java convention we will follow is AOSP. You can check it here:

https://source.android.com/setup/contribute/code-style
