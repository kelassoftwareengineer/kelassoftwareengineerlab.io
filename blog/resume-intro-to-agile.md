# Resume - Intro to Agile

![Thumbnail](../image/agile-games-img-1.jpg "Mentors and Participants")

## Background

**Agile** known as a software development with a series of iterations.
But further, it can be applied to team/projects to ensure agility, flexibility, and adaptability.
We create this class so that participants know practical understanding, principles, and concepts of Agile.
The class will be focused on a practical exercise of agile concept on managing projects.

## Agile Games

![Agile Games](../image/agile-games-img-2.JPG "Mentors is explaining estimation phase to participants")

> Practices make perfect

In order to understand what agile really is, we use games as media to learn.
The plot is we are a group of developer that has a project to create a fantasy zoo in our city.
Then the class divided into 3 groups which are Cairo, London, and Jakarta.
Each of them has one mentor that acts as a client.
This client not only watches their work but also give them criticism and guidance
about agile and the way they work. There is also a facilitator that drives
this game so that the atmosphere and the pace will feel like real agile development.

## Lesson Learned

![Final product](../image/agile-games-img-3.png "Final product after 3rd iteration")

- **What was the learning that you got in each iteration?** At the first iteration,
we underestimate the stories because we cannot measure the difficulty of the stories
and don't really know about our resources. At the second iteration, there is
a plot twist that occurs but as we get used to it, we can divide the task
and can end this iteration well. In the third iteration, we become mature and be ready
for any situation that may happen.

- **How important is the cycle? Explain why?** Estimation phase is for allocating the resources and
measure the difficulty of stories, Sign up phase is for listening and clarifying what client needs while
have a negotiation with them, Development phase is to execute the plan, Showcase phase is for presenting
the progress to the client and Retro phase is to collect feedback about what went well in this iteration
and what needs to be improved for the next iteration.

- **How the interaction with the client & team goes? What should be the ideal one?**
Participants should include the client in every iteration so that our
final product meets his/her needs. But only that is not enough, participants also need to clarify
and document the contract that signed up in each iteration.

## Learning Materials

- Agile Handbook: http://agilehandbook.com/agile-handbook.pdf

- Slide: http://bit.ly/kelasse-slide-intro-to-agile

## End words

**Agile** is more than tool/framework. It is a mindset of working fast, can adapt to change, and open to feedback.

I think that's all. I hope it will useful for you.

Cheers, With ♥️ from Ciwaruga

[@qornanali](https://gitlab.com/qornanali)
