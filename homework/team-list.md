# Team List

| Team Number | Member 1                   | Member 2                   | Member 3                                | Mentor                |
|----|-----------------------------|-----------------------------|------------------------------------------|-----------------------|
| 1  | Alif Ramdani                | Elza Esterina               | Dwiana Kamila A.S                        | Ali Qornan            |
| 2  | Ananda Zukhruf Awalwi       | Ivan Eka Putra              | Dewanto Joyo Pramono                     | Ali Qornan            |
| 3  | Muhamad Wahyu Maulana Akbar | Rezky Wahyuda Sitepu        | Egi Nurfikri                             | Asep Wildan           |
| 4  | Refdinal Tubagus            | Nurallisha Rahmanda         | Ahmad Aji Naufal Ali                     | Bicky Eric            |
| 5  | Cahya Ramadhan              | Salma Meldiyana             | Chofief Al Farroqie Ariestotles Endamara | Bicky Eric            |
| 6  | Ali Piqri                   | Rayhan Azka Anandhias Putra | Yoga                                     | Fajar Garnadi         |
| 7  | Muhammad Jaysy Ansharulloh  | Adhitya Febhiakbar          | Rindu Mustika                            | Fajar Garnadi         |
| 8  | M. Dena Adryan              | Naufal Rajabi               | Agung Tri A                              | Imam Fauzan           |
| 9  | Darul Ismawan               | Fajrina Aflaha              | Riyanzani A.P                            | Imam Fauzan           |
| 10 | Rizky Wahyudi               | Irvan Kadhafi               | Rahmadi                                  | Mughie Arief          |
| 11 | Rifqi Oktabhiar Erawan      | Aris Purnomo                | Hamzah Prasetio Utomo                    | Mughie Arief          |
| 12 | Kiki Pratiwi                | Mufida Nuha                 |                                          | Muhammad Saiful Islam |
| 13 | Alwan Assyauqi              | Rifqi Yuner                 | Istamar Siddiq                           | Muhammad Saiful Islam |
| 14 | Adi Maulana Triadi          | Dinan Rangga Maulana        |                                          | Ridwan Fathin         |