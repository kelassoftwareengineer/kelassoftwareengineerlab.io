# Homework Week 1

1. [Download](https://git-scm.com/downloads) Git and install the executable file.
2. [Download](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) Java 8 and follow this [instruction](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html).
3. Install Gradle by following this [instruction](https://gradle.org/install/).
4. Check software versions by following this [guide](https://docs.google.com/document/d/1gp12rdOI8PuFjKOFkB6IR2SBDE99sEYTyrGrd-2l6wM/edit?usp=sharing).
5. Install Visual Studio Code by following this [instruction](https://code.visualstudio.com/docs/setup/setup-overview).
6. Create Gitlab account then join this group. You can follow this [guide](../tutorial/joining-gitlab-group.md).
7. Read blog ["Intro to Git"](https://hackernoon.com/a-gentle-introduction-to-git-and-github-the-eli5-way-43f0aa64f2e4) by Hackernoon.
8. Read blog ["Introduction to TDD"](https://hackernoon.com/introduction-to-test-driven-development-tdd-61a13bc92d92) by Hackernoon.
9. Write lesson learned that you got in Week 1. Please follow this [guide](../tutorial/writing-lesson-learned.md). (*Not mandatory*)
