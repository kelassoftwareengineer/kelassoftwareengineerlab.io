# Homework Week 3

1. Continue to work on your Java Project by implementing all thing you have learned in previous week.
Please complete all stories you have been taken and push your project to [week 3](https://gitlab.com/kelassoftwareengineer/week3)
before thursday, 5 December 2019 21.00 so that mentors can review your merge request.
2. When doing TDD, a git commit should be small. You committed every time you change something and your test has been passed **DONT MAKE A ONE BIG COMMIT**.
Why should we commit often and it should be small? Please read these:
  - [Why a small atomic commit is easier to work with](https://medium.com/@fagnerbrack/one-commit-one-change-3d10b10cebbf)
  - [Why Use a Version Control System?](https://www.git-tower.com/learn/git/ebook/en/desktop-gui/basics/why-use-version-control)
3. Read blog ["SOLID"](https://hackernoon.com/solid-principles-made-easy-67b1246bcdf) by Hackernoon.
4. Read about .gitignore
5. Find out the importance of README
6. Install [Tig](https://jonas.github.io/tig/INSTALL.html).
7. Write lesson learned that you got in Week 3. Please follow this [guide](../tutorial/writing-lesson-learned.md). (*Not mandatory*)

## Note

- We got a lot of feedback about to want the homework to get reviewed but many submissions not follow the contribution guideline and not submitted it early. So make sure to do it properly.
You can see an example of good contribution
process in this merge request: https://gitlab.com/kelassoftwareengineer/kelassoftwareengineer.gitlab.io/merge_requests/6.
