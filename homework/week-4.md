# Homework Week 4

1. Continue to work on your Java project by implementing all you have learned in
   the previous week.
2. Do all the previous homeworks ([week 1][1], [week 2][2], [week 3][3])
and push it to group [week 4][6] before 12 December 2019 21.00 so that mentors can review it for you.
3. Implement [google-java-format-gradle-plugin][4] and use AOSP format.
   * Refer to [this repository][5] for a good example.
   * Don't forget to update your `README.md` file to include the linter
     commands.
4. Understand what are the differences between the `verGJF` and `goJF` command
   in the linter (i.e. what are the commands doing?).
5. Exclude unnecessary files in your repository by utilizing gitignore.
6. Read blog about [Continuous Integration][7] by Martin Fowler and [Continuous Delivery][8] by Jez Humble.
7. Write lesson learned that you got in Week 4. Please follow this [guide](../tutorial/writing-lesson-learned.md). (*Not mandatory*)

## Prerequisites for Week 5

In week 5, we will learn about CI/CD and practice it. We will use **your team
repository**, and we will do Git push/pull in this class.

To prepare for that, make sure:

* We already have our project ready in GitLab.
  * In the class, we will not help setting up local environment again - please
    check with your classmates :)
* You know how to set up JTK proxy for your `git` and `gradle`.
  * Discuss with your classmates for this.
  * How to check for this:
    * We can do `git push` and `git pull` in JTK to your repository.
    * We can do `gradle init` and `gradle test` **straight from an empty
      folder** in JTK.
* You already set up a linter (assigned as homework above).

[1]: week-1.md
[2]: week-2.md
[3]: week-3.md
[4]: https://github.com/sherter/google-java-format-gradle-plugin
[5]: https://gitlab.com/kelassoftwareengineer/week2/qornanali_todolistapp
[6]: https://gitlab.com/kelassoftwareengineer/week4
[7]: https://martinfowler.com/articles/continuousIntegration.html
[8]: https://continuousdelivery.com/
