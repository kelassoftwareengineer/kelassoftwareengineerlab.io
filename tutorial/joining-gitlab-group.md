# Joining Gitlab Group

1. Visit the gitlab group page https://gitlab.com/groups/kelassoftwareengineer/

2. Click "Request Access" under the Group title ![step 1](../image/join-gitlab-group-step-1.jpeg)

3. Waiting 😆 ![step 2](../image/join-gitlab-group-step-2.jpeg)

4. Check your email and yeeaayy.. you got to access to the group.. 🎉 ![step 3](../image/join-gitlab-group-step-3.jpeg)
